import json
import boto3

def triggerEvent(event, context):
    body = {
        "message": "Executing trigger for activate all partners!",
        "input": event,
    }

    #detail = 

    client = boto3.client('events')
    response = client.put_events(
        Entries=[
            {
                'DetailType': 'activate',
                'Source': "custom.activationManager",
                'Detail': json.dumps(hanufitDetail())
            },
            {
                'DetailType': 'activate',
                'Source': "custom.activationManager",
                'Detail': json.dumps(zenDetail())
            },
            {
                'DetailType': 'activate',
                'Source': "custom.activationManager",
                'Detail': json.dumps(customDetail())
            }
        ]
    )
    print(response)

    response = { 
        "statusCode": 200, 
        "body": json.dumps(response)
    }
    

    return response


def customDetail():
    return {
        'sub': "123456",                    
        'first_name': "nombre",
        'last_name': "apellido1",
        'maternal_last_name': "apellido2",
        'email': "prueba@gobetterfly.com",
        'local_identifier': {
            'id_type_id': 2,
            'id_type': "cpf",
            'id_value': "35032029855"
        },
        'birthdate': '1978-01-01',
        'gender': 'male',
        'phone_number': '+56911111111',
        'plan': {
            'id': 3,
            'start_date': '2021-12-01'
        },
        'locale': 'en',
        'partner': [
            'conexa'
        ]
    }

def hanufitDetail():
    return {
        'sub': "123456",                    
        'first_name': "nombre",
        'last_name': "apellido1",
        'maternal_last_name': "apellido2",
        'email': "prueba@gobetterfly.com",
        'local_identifier': {
            'id_type_id': 2,
            'id_type': "cpf",
            'id_value': "35032029855"
        },
        'birthdate': '1978-01-01',
        'gender': 'male',
        'phone_number': '+56911111111',
        'plan': {
            'id': 3,
            'start_date': '2021-12-01'
        },
        'locale': 'en',
        'partner': [
            'hanufit'
        ]
    }

def zenDetail():
    return {
        'sub': "123456",                    
        'first_name': "nombre",
        'last_name': "apellido1",
        'maternal_last_name': "apellido2",
        'email': "prueba@gobetterfly.com",
        'local_identifier': {
            'id_type_id': 2,
            'id_type': "cpf",
            'id_value': "35032029855"
        },
        'birthdate': '1978-01-01',
        'gender': 'male',
        'phone_number': '+56911111111',
        'plan': {
            'id': 3,
            'start_date': '2021-12-01'
        },
        'locale': 'en',
        'partner': [
            'zen'
        ]
    }
